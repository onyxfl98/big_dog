big_dog
=======
Stack for various robot projects.

- **docs**
| Notes about the status of the overall projects

- **intellitec_parts**
| Part descriptions for parts that will be used for pick and place

- **niryo**
| Niryo One arm, end effectors, pick and place code, etc

- **panda**
| Franka Panda arm code

- **shared**
| models, launch files, and other assets shared between projects

- **widow_x**
| Widow X arm pick and place code