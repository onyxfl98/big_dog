Notes
=====

widow_x/widowx_gazebo_moveit/widowx_gazebo/package.xml has an error in it. 
`<run_depend>widowx_description</run_depend>` should be `<run_depend>widowx_arm_description</run_depend>`

When you clone the repo, unless your username on your computer is onyx you will need to redo the symlink in niryo/niryo_my_pack/meshes to point to your location for niryo/niryo_one/ STL

**Launches**

Niryo MoveIt with Gazebo controllers. runs plans and executes but doesn't control Gazebo model.  
`
roslaunch niryo_one_moveit_config demo.launch fake_execution:=false
`

Niryo Gazebo. Currently no end effector.  
`
roslaunch shared spawn_xacro.launch model:=$(rospack find niryo_my_pack)/niryo_one.urdf.xacro
`

Niryo rViz only  
`
roslaunch shared rviz_xacro.launch model:=$(rospack find niryo_my_pack)/niryo_one.urdf.xacro
`

Intellitec Parts  
`
roslaunch intellitec_parts spawn_part.launch model:=[part] instance:=[counter] paused:=true
`
example: `roslaunch intellitec_parts spawn_part.launch model:=04_washer instance:=0 paused:=true`
