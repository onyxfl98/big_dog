Given masses in Grams.
Given lengths in Millimeters

Calculating Volume and Unscaled Inertias in MeshLab 2016.12

Calculating Scaled Inertias as X / Scale<sup>5</sup> / Unscaled Volume * (Given Mass / 1000)

Scaled COM is the inertial origin xyz of part in URDF/XACRO

[New calculation sheet](https://docs.google.com/spreadsheets/d/18cLp0knsuZgEkwCtweESqWH9ZWuOhDBKc6JNLhbx7Fo/edit?usp=sharing)

I think we may need to recalculate these and scale everything up. If I use inertias in the e-17 range or even up to e-8 range Gazebo struggles to handle them properly. 