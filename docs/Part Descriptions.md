| | Part Number | Description | Mass |
 --- | --- | --- | ---:
 1 | 03-50623-016 | #6 x 1/2 phil pan stl/znc screw | 1.0 gm
2 | 13-03298-000 | label BD relay 1.31 x .85 |
3 | 49-00002-002 | solenoid-anti rot studs 12v x opd | 247.8 gm
4 | 41-01003-000 | spring, return | 0.4 gm 
5 | 49-00024-001 | latch housing assembly w/o fuses | 42.0 | gm
6 | 49-00078-000 | Plunger Assy #2 | 42.5 gm

---

| | Part Number | Description | Mass |
 --- | --- | --- | ---:
| | 49-00024-001 | latch housing assembly w/o fuses
1 | 05-24004-000 | pivot pin .125DIAx.73 | 1.2 gm
2 | 05-25003-000 | inter push on 7/8 | 0.3 gm
3 | 15-00111-000 | relay cap battery disconnect | 34.2 gm*
4 | 49-00056-000 | latch pivot magnet assembly | 6.3 gm (3.8 brass, 2.4 magnet)
* all other parts weighed

---

| | Part Number | Description | Mass |
 --- | --- | --- | ---:
| | 49-00078-000 | Plunger Assy #2
1 | 04-02576-030 | washer flat .762OD .275ID .0312 | 0.8 gm*
2 | 05-25002-000 | E-ring 1/4  | 0.3 gm
3 | 40-00002-004 | contact washer | 14.2 gm
4 | 40-00002-006 | insulating washer | 1.1 gm
5 | 40-00002-009 | insulating washer DB relay | 0.5 gm
6 | 41-01010-001 | Spring over travel .365 | 1.2 gm
7 | 45-00022-000 | Plunger ACC solenoid DB RLY | 26.0 gm
* all other parts weighed